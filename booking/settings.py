from django.conf import settings


PAGINATION = getattr(settings, 'DJ_BOOKING_PAGINATION', 10)

BOOKING_SUCCESS_REDIRECT_URL = getattr(settings, 'BOOKING_SUCCESS_REDIRECT_URL', None)

BOOKING_DISABLE_URL = getattr(settings, 'BOOKING_DISABLE_URL', "/")

BOOKING_BG = getattr(settings, 'BOOKING_BG', "img/booking_bg.jpg")

BOOKING_TITLE = getattr(settings, 'BOOKING_TITLE', "Rezervacija")

BOOKING_DESC = getattr(settings, 'BOOKING_DESC', "Napravite rezervaciju brzo i jednastavno sa nama.")

DATE_INPUT_FORMATS = ['%m-%d-%Y', '%m-%d-%Y' ] # '10-28-2021'

USE_L10N = False # Here