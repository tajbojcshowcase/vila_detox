from django.urls import path

from .views import (BookingApproveView, BookingCreateWizardView,
                    BookingDeleteView, BookingHomeView, BookingListView,
                    BookingSettingsView, get_available_time)

urlpatterns = [
    path("booking", BookingCreateWizardView.as_view(), name="create_booking"),
    path("booking/admin", BookingHomeView.as_view(), name="admin_dashboard"),
    path("booking/admin/list", BookingListView.as_view(), name="booking_list"),
    path("booking/admin/settings", BookingSettingsView.as_view(), name="booking_settings"),
    path("booking/admin/<pk>/delete",
         BookingDeleteView.as_view(), name="booking_delete"),
    path("booking/admin/<pk>/approve",
         BookingApproveView.as_view(), name="booking_approve"),
    path("get-available-time", get_available_time, name="get_available_time"),
]