from django.shortcuts import render, redirect
from core.models import Contact
from django.contrib import messages



# Create your views here.

def Home(request):
    return render(request, 'core/index.html')
 
def about(request):
    return render (request, 'core/about.html')

def room(request):
    return render (request, 'core/room.html' )

def gallery(request):
    return render (request, 'core/gallery.html')

def blog(request):
    return render (request, 'core/blog.html')



def contact(request):
    if request.method=="POST":
        
        name=request.POST.get('name')
        email=request.POST.get('email')
        phone_number=request.POST.get('phone_number')
        message=request.POST.get('message')
        myquery=Contact(name=name, email=email, phone_number=phone_number, message=message)
        myquery.save()

        messages.success(request, "Thanks for contacting us. We'll get back to you soon.")
        return redirect('contact') 

        #messages.info(request,"Thanks for contacting us, we will get back you soon")
    return render(request, 'core/contact.html')






