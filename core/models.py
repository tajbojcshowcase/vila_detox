from django.db import models


# Create your models here.

class Contact(models.Model):
    name=models.CharField(max_length=200)
    email=models.EmailField()
    phone_number=models.IntegerField()
    message=models.TextField()

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):  #nesto za produkciju
        super().save(args, **kwargs)




