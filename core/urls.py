from django.urls import path
from . import views

urlpatterns = [
    path('', views.Home, name='home' ),
    path('about', views.about, name='about'),
    path('room', views.room, name='room'),
    path('gallery', views.gallery, name='gallery'),
    path('blog', views.blog, name='blog'),
    path('contact', views.contact, name='contact'),


    
] 